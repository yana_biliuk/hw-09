// task 1

const elementClass = document.getElementsByClassName("feature");

for (let i = 0; i < elementClass.length; i++) {
  elementClass[i].style.textAlign = "center";
}
console.log(elementClass);

// Спосіб ІІ
const elementByQuery = document.querySelectorAll("feature");

elementByQuery.forEach((element) => {
  element.style.textAlign = "center";
});
console.log(elementByQuery);

// task 2

const titleElement = document.querySelectorAll("h2");

titleElement.forEach((element) => {
  element.textContent = "Awesome feature";
});



// task 3

const title = document.querySelectorAll('.feature-title');


title.forEach(title => {
    title.textContent += '!';
});